﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exception2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] mas = { 1, 2, 3, 4, 5 };
            int numberOfIteration = 10;
            for (int i = 0; i < numberOfIteration; i++)
            {
                try
                {
                    Console.WriteLine("Enter i: ");
                    int j = int.Parse(Console.ReadLine());
                    Console.WriteLine(mas[j]);
                    if (j > mas.Length)
                    {
                        throw new IndexOutOfRangeException();
                    }
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.ReadLine();
            Console.ReadLine();

        }
    }
}
